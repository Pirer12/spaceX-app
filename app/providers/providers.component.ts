import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';



@Component({
	moduleId: module.id,
	selector: 'providers',
	templateUrl: 'providers.component.html'
})

@Injectable()
export class ProvidersComponent {

	constructor(
		public http: HttpClient,
	) 
	{
	}

	getLaunches(){
		return this.http.get("https://api.spacexdata.com/v2/launches")
	}
	
	getMission(missionId){
		return this.http.get(`https://api.spacexdata.com/v2/launches/all?flight_number=${missionId}`)
	}

	getMyApi(){
		return this.http.get("https://pirerservice-api.cfapps.io/getAll")
	}

	setDateToApi(firstName, lastName, age, body){
		return new Promise((resolve, reject)=>{ this.http.post(`https://pirerservice-api.cfapps.io/create?firstName=${firstName}&lastName=${lastName}&age=${age}`,body).subscribe((data: any) => {
			resolve(data)
		}, err => reject(err))
		})
	}
}