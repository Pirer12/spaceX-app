import { Component, OnInit } from '@angular/core';
import  {ActivatedRoute } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";
import { AccordionModule } from "nativescript-accordion/angular";
import { ProvidersComponent } from '~/providers/providers.component';


@Component({
	selector: 'launches',
	moduleId: module.id,
	templateUrl: 'launches.component.html',
	styleUrls:['./launches.component.css']
})

export class LaunchesComponent implements OnInit {
	public missionId: any;
	public mission: any;
	processing = true;


	constructor(
		private routerExtensions: RouterExtensions,
		private route: ActivatedRoute,
		public dataservices: ProvidersComponent
	){
		this.route.params.subscribe((params)=>{
			this.missionId = params["mission"];
		})
	}

	ngOnInit() {
		this.dataservices.getMission(this.missionId).subscribe((response: any) => {
			this.mission = response[0];
			this.processing = false;
		})
	}

	// getMission(){
    //     fetch(`https://api.spacexdata.com/v2/launches/all?flight_number=${this.missionId}`)
    //     .then((response) => response.json())
    //     .then((result) => {
	// 		this.mission = result[0];
	// 		this.processing = false;
    //         console.log(this.mission)
    //     }).catch((err) => {
    //     });
    // }

	goBack() {
		this.routerExtensions.back();
		console.log("Back")
	}
}