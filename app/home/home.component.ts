import { Component, OnInit,NgModule, ViewChild, ChangeDetectorRef, AfterViewInit } from "@angular/core";
import { Router } from "@angular/router";
import * as app from "application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { HttpClient} from '@angular/common/http';
import { ProvidersComponent } from "~/providers/providers.component";
import { RouterExtensions } from "nativescript-angular/router";


@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls:['./home.component.css']
})

export class HomeComponent implements OnInit{
    public launches = [];
    public processing = true;
    
    constructor(
        public dataservice: ProvidersComponent,
        private router: Router,
        public http: HttpClient,
        public routerExtension: RouterExtensions
    ) {
    }

    ngOnInit(): void {
        // Init your component properties here.
        console.log("Hiii");
        this.dataservice.getLaunches().subscribe((response: any) => {
            this.launches = response;
            this.processing = false;
        })
    }

    // getLaunches(){
    //     fetch("https://api.spacexdata.com/v2/launches")
    //     .then((response) => response.json())
    //     .then((result) => {
    //         this.launches = result;
    //         this.processing = false;
    //         console.log(this.launches)
    //     }).catch((err) => {
    //     });
    // }

    onTap(item) {
        console.log("Go")
            this.routerExtension.navigate(["launches",item],{
                transition: {
                    name: "slideLeft",
                    duration: 500,
                    curve: "easeInOut"
                }
            })
      }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

}

