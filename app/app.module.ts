import { NgModule, NgModuleFactoryLoader, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LaunchesComponent } from "~/launches/launches.component";
import { AccordionModule } from "nativescript-accordion/angular";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { HttpClientModule } from '@angular/common/http'; 
import { ProvidersComponent } from '~/providers/providers.component';
import { TestComponent } from "~/test/test.component";



@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        HttpClientModule,
        NativeScriptModule,        
        AppRoutingModule,
        AccordionModule,
        NativeScriptUISideDrawerModule,
        NativeScriptRouterModule,
        NativeScriptCommonModule,
    ],
    declarations: [
        AppComponent,
        LaunchesComponent,
        TestComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    providers: [
        HttpClientModule,
        ProvidersComponent
    ]
})
export class AppModule { }
