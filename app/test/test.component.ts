import { Component, OnInit } from '@angular/core';
import { ProvidersComponent } from '~/providers/providers.component';
import { RouterExtensions } from 'nativescript-angular/router';

@Component({
	moduleId: module.id,
	selector: 'test',
	templateUrl: 'test.component.html'
})

export class TestComponent implements OnInit {
	public test: any

	constructor(
		private dataservice: ProvidersComponent,
		private routerExtensions: RouterExtensions
	){}

	ngOnInit() { 
		this.dataservice.getMyApi().subscribe((response: any) =>{
			this.test = response
			console.log(response)
		})
	}
	goBack() {
		this.routerExtensions.back();
		console.log("Back")
	}
	showAlert(result) {
		alert("Text: " + result);
		this.dataservice.setDateToApi(result,"LastTest",12,"test")
    }
}